import uuid

from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.db import models
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

User = get_user_model()


class WalletModel(models.Model):
    class Meta:
        verbose_name = _("Кошелек")
        verbose_name_plural = _("Кошельки")

    user = models.OneToOneField(
        verbose_name=_("Пользователь"),
        to=User,
        related_name='wallet',
        on_delete=models.CASCADE
    )
    balance = models.IntegerField(
        verbose_name=_("Баланс"),
        editable=False, default=0
    )

    def __str__(self):
        return _("Кошелек пользователя {0}").format(self.user)


class TransactionsModel(models.Model):
    class Meta:
        verbose_name = _("Транзакция"),
        verbose_name_plural = _("Транзакции")

    id = models.UUIDField(
        verbose_name=_("Идентификатор"),
        primary_key=True,
        editable=False,
        default=uuid.uuid4
    )
    desc = models.TextField(
        verbose_name=_("Описание"),
        blank=True, null=True
    )
    created_date = models.DateTimeField(
        verbose_name=_("Дата создания"),
        editable=False, default=timezone.now
    )
    amount = models.PositiveIntegerField(
        verbose_name=_("Сумма транзакции")
    )
    from_account = GenericForeignKey(
        'content_type_from_account', 'from_account_id',
        for_concrete_model=False
    )
    to_account = GenericForeignKey(
        'content_type_to_account', 'to_account_id',
        for_concrete_model=False
    )

    # Content types property
    from_account_id = models.PositiveIntegerField(verbose_name=_("Отправитель"))
    content_type_from_account = models.ForeignKey(ContentType, related_name="outgoing_transactions", on_delete=models.CASCADE)

    to_account_id = models.PositiveIntegerField(verbose_name=_("Получатель"))
    content_type_to_account = models.ForeignKey(ContentType, related_name="incoming_transactions", on_delete=models.CASCADE)

    def __str__(self):
        return _("Транзакция #{0} из {1} в {2}").format(self.id.hex[:6], self.from_account, self.to_account)
