from social_sweepstakes.signals import created_bet, increased_bet, withdrawn_bet
from social_sweepstakes.logic.bet import Bet
from social_sweepstakes.wallet.logic import Transactions
from django.dispatch import receiver


@receiver(increased_bet, sender=Bet)
@receiver(created_bet, sender=Bet)
def create_or_update_bet_handler(sender, *args, bet: Bet = None, **kwargs) -> None:
    """
    This handler creating transaction after sending update or create bet signal
    :param sender: Bet
    :param args: inherit
    :param bet: Initiator instance (Bet)
    :param kwargs: inherit
    :return: None
    """
    Transactions.create_transaction(
        from_acc=bet.user.wallet,
        to_acc=bet.event,
        amount=bet.history[-1]['amount']
    )


@receiver(withdrawn_bet)
def withdrawn_bet_handler(sender, *args, bet: Bet = None, **kwargs) -> None:
    """
    This handler creating transaction after sending withdrawn bet signal (for rollback many)
    :param sender: Bet
    :param args: inherit
    :param bet: Initiator instance (Bet)
    :param kwargs: inherit
    :return: None
    """
    Transactions.create_transaction(
        from_acc=bet.event,
        to_acc=bet.user.wallet,
        amount=bet.amount
    )
