from django.apps import AppConfig


class WalletConfig(AppConfig):
    name = 'social_sweepstakes.wallet'

    def ready(self):
        # Register handlers
        from .handlers.wallets import *
