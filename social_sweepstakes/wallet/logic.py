import logging
from social_sweepstakes.models import WalletModel, TransactionsModel
from social_sweepstakes.settings import conf
from django.core.exceptions import FieldDoesNotExist
from django.db import transaction, DatabaseError


logger = logging.getLogger(__name__)


class Wallet(WalletModel):
    class Meta:
        proxy = True
        managed = True


class Transactions(TransactionsModel):
    class Meta:
        proxy = True
        managed = True

    @classmethod
    def create_transaction(cls, from_acc, to_acc, amount: int, desc: str = None) -> TransactionsModel:
        """
        This method created transaction
        :param from_acc: From account (Django model instance)
        :param to_acc: To account (Django model instance)
        :param amount: Amount transaction (int)
        :param desc: Transaction description (optional)
        :return: TransactionModel (TransactionModel)
        """
        if not hasattr(from_acc, 'balance') or not hasattr(to_acc, 'balance'):
            raise FieldDoesNotExist("The {0} model not have a 'balance' field.".format(
                from_acc.__class__.__name__ if not hasattr(from_acc, 'balance') else to_acc.__class__.__name__))

        with transaction.atomic():
            instance = cls.objects.create(
                from_account=from_acc,
                to_account=to_acc,
                amount=amount,
                desc=desc,
            )
            if from_acc.balance < amount and not conf.SOC_SWEEP_WALLET_CAN_OVERDRAFT:
                raise ValueError("In {0} account insufficient funds for transaction. Balance: {1}. Transaction: {2}."
                                 "If you want to allow such transactions - specify in the settings "
                                 "'SOC_SWEEP_WALLET_CAN_OVERDRAFT = True'")

            from_acc.balance -= instance.amount
            to_acc.balance += instance.amount
            from_acc.save()
            to_acc.save()

        return instance

    def rollback_transaction(self) -> bool:
        """
        This method rollback transaction and revert of balance participants
        :return: Rollback status (bool)
        """
        try:
            with transaction.atomic():
                instance = self
                self.delete()

                self.from_account.balance += instance.amount
                self.to_account.balance -= instance.amount
                self.from_account.save()
                self.to_account.save()
            return True
        except DatabaseError as e:
            logger.error(e)
            return False
