from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from jsonfield import JSONField

from social_sweepstakes.models.event import EventModel
from social_sweepstakes.settings import conf

User = get_user_model()


class BetModel(models.Model):
    class Meta:
        verbose_name = _("Ставка")
        verbose_name_plural = _("Ставки")

    user = models.ForeignKey(
        verbose_name=_("Игрок"),
        to=User,
        related_name='bets',
        on_delete=models.CASCADE
    )
    event = models.ForeignKey(
        verbose_name=_("Связанное событие"),
        to=EventModel,
        related_name='bets',
        on_delete=models.CASCADE
    )
    desc = models.TextField(
        verbose_name=_("Описание ставки"),
        blank=True, null=True
    )
    forecast = models.BooleanField(
        verbose_name=_("Прогнозируемый итог события"),
        editable=False, default=conf.DEFAULT_BET_FORECAST
    )
    amount = models.PositiveIntegerField(
        verbose_name=_("Величина ставки"),
    )
    gain = models.PositiveIntegerField(
        verbose_name=_("Выйгрыш ставки"),
        editable=False, null=True
    )
    created_date = models.DateTimeField(
        verbose_name=_("Дата создания ставки"),
        editable=False, default=timezone.now
    )
    updated_date = models.DateTimeField(
        verbose_name=_("Дата обновления ставки"),
        editable=False, null=True
    )
    is_withdrawn = models.BooleanField(
        verbose_name=_("Снята ли ставка"),
        blank=True, default=False
    )
    history = JSONField(
        verbose_name=_("История обновлений ставки"),
        default=[], null=True, blank=True
    )

    def __str__(self):
        return "{0} bet to {1} event".format(self.user, self.event.title)

    @property
    def gain_with_commission(self) -> int:
        """
        This property returned gain with commission
        :return: Gain (int)
        """
        return self.gain - ((self.gain * conf.WINNING_COMMISSION_PERCENT) * 100)

    @property
    def get_gain(self) -> int:
        """
        This property return gain by event gain size and winners count
        :return: gain (int)
        """
        gain_size = self.event.get_clear_gains()[0]
        winners_count = self.event.winner_bets.count()

        return int(self.amount + (int(gain_size) / int(winners_count)))
