from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

from social_sweepstakes.models.event import EventModel


def proof_file_upload_to(instance, filename: str) -> str:
    """
    This function return file path for uploading file
    :param instance: Proof instance (Proof)
    :param filename: File name (str)
    :return: File path (str)
    """
    return "proofs/{0}/{1}".format(instance.pk, filename)


class ProofModel(models.Model):
    class Meta:
        verbose_name = _("Пруф"),
        verbose_name_plural = _("Пруфы")

    event = models.ForeignKey(
        verbose_name=_("Событие"),
        to=EventModel,
        related_name='proofs',
        on_delete=models.CASCADE
    )
    title = models.CharField(
        verbose_name=_("Заголовок для пруфа"),
        max_length=100
    )
    desc = models.TextField(
        verbose_name=_("Описание пруфа"),
        blank=True, null=True
    )
    link = models.URLField(
        verbose_name=_("Ссылка на пруф"),
        blank=True, null=True
    )
    file = models.FileField(
        verbose_name=_("Файл пруфа"),
        null=True, blank=True,
        upload_to=proof_file_upload_to
    )

    def __str__(self):
        return _("Пруф для {0}: {1}").format(self.event.title, self.title)
