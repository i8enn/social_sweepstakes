from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from social_sweepstakes.settings import conf


User = get_user_model()

# Event statuses
NEW = 0
ACCEPTED_BETS = 1
NOT_ACCEPTED_BETS = 2
IN_CHECKING = 3
END_EVENT = 4
EVENT_STATUSES = (
    (NEW, _(u"Новое")),
    (ACCEPTED_BETS, _(u"Принимаются ставки")),
    (NOT_ACCEPTED_BETS, _(u"Ставки не принимаются")),
    (IN_CHECKING, _(u"На проверке")),
    (END_EVENT, _(u"Закончено"))
)
DEFAULT_EVENT_STATUS = NEW


class EventModel(models.Model):
    class Meta:
        verbose_name = _(u"Событие")
        verbose_name_plural = _(u"События")
        ordering = ['created_date']

    user = models.ForeignKey(
        verbose_name=_(u"Автор"),
        to=User,
        related_name=u'events',
        on_delete=models.CASCADE
    )
    title = models.CharField(
        verbose_name=_(u"Название события"),
        max_length=255
    )
    desc = models.TextField(
        verbose_name=_(u"Подробности события"),
        blank=True, null=True
    )
    status = models.PositiveIntegerField(
        verbose_name=_(u"Статус события"),
        choices=EVENT_STATUSES,
        blank=True, default=DEFAULT_EVENT_STATUS
    )
    result = models.NullBooleanField(
        verbose_name=_(u"Результат события"),
        blank=True, default=None,
        help_text=_(u"Результат события устанавливается модератором после завершения события."
                    "Если результат - True - значит автор выдержал пари, и соответственно если нет - False")
    )
    balance = models.PositiveIntegerField(
        verbose_name=_("Банк события"),
        editable=False, default=conf.DEFAULT_EVENT_BALANCE,
        help_text=_(u"Количество денег поставленных на событие")
    )

    created_date = models.DateTimeField(
        verbose_name=_(u"Дата создания события"),
        editable=False, default=timezone.now
    )
    published_date = models.DateTimeField(
        verbose_name=_(u"Дата публикации события"),
        editable=False, null=True
    )
    end_bets_date = models.DateTimeField(
        verbose_name=_("Дата окончания приема ставок"),
        blank=True, null=True
    )
    end_date = models.DateTimeField(
        verbose_name=_("Дата окончания события")
    )

    def __str__(self):
        return "{0} [{1}]".format(self.title, self.get_status_display())

    @property
    def is_published(self):
        return self.status in [ACCEPTED_BETS, NOT_ACCEPTED_BETS, IN_CHECKING, END_EVENT]

    @property
    def winner_bets(self):
        """
        This method returns winning bets
        :return: Winning bets (QS or None)
        """
        if self.status != END_EVENT:
            return None
        return self.bets.filter(forecast=self.result)

    @property
    def losing_bets(self):
        """
        This property returns losing bets
        :return: Losing bets (QS or None)
        """
        if self.status != END_EVENT:
            return None
        return self.bets.exclude(forecast=self.result)

    def get_clear_gains(self) -> set or None:
        """
        This method returns gains and count losing bets in set
        If event not closing - returns None
        :return: Losing bet count and event gains (without rollback win bets)
        """
        if self.status != END_EVENT:
            return None
        losing_bets = self.bets.exclude(forecast=self.result)
        return (
            losing_bets.aggregate(models.Sum('amount'))['amount__sum'],
            losing_bets.count()
        )
