import os
import datetime
import pkg_resources


CREATE_YEAR = 2018
VERSION = pkg_resources.get_distribution('social_sweepstakes').version

__title__ = "Social Sweepstakes"
__version__ = "-".join(VERSION)
__author__ = "Ivan Galin"
__license__ = "Apache License"
__copyright__ = "Copyright {0} Ivan Galin".format(
    CREATE_YEAR if datetime.datetime.now().year == CREATE_YEAR
    else "{0}-{1]".format(CREATE_YEAR, datetime.datetime.now().year))
