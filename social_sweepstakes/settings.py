from __future__ import absolute_import

from django.conf import settings


class Settings(object):

    # wallet setting
    @property
    def SOC_SWEEP_WALLET_CAN_OVERDRAFT(self):
        return getattr(settings, 'SOC_SWEEP_WALLET_CAN_OVERDRAFT', False)

    # bet setting
    @property
    def DEFAULT_BET_FORECAST(self):
        return getattr(settings, 'SOC_SWEEP_DEFAULT_BET_FORECAST', False)

    @property
    def WINNING_COMMISSION_PERCENT(self):
        return getattr(settings, 'SOC_SWEEP_WINNING_COMMISSION_PERCENT', 10)

    # event settings
    @property
    def DEFAULT_EVENT_BALANCE(self):
        return getattr(settings, 'SOC_SWEEP_DEFAULT_EVENT_BALANCE', 0)

    # proof settings
    @property
    def PROOF_ALLOWED_FILE_EXTENSIONS(self):
        return getattr(settings, 'SOC_SWEEP_PROOF_ALLOWED_FILE_EXTENSIONS', [
            'jpeg', 'png', 'csv', 'docx', 'odt', 'rtf', 'txt', 'xlsx', 'ods'])


conf = Settings()
