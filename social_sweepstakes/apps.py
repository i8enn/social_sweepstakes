from django.apps import AppConfig


class SocialSweepstakesConfig(AppConfig):
    name = 'social_sweepstakes'

    def ready(self):
        pass
