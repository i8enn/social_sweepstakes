import logging

from django.contrib.auth import get_user_model
from django.core.exceptions import PermissionDenied
from django.db import transaction, DatabaseError
from django.utils import timezone

from social_sweepstakes.logic.event import Event
from social_sweepstakes.models import BetModel
from social_sweepstakes.signals import created_bet, increased_bet, withdrawn_bet

User = get_user_model()
logger = logging.getLogger(__name__)


class Bet(BetModel):
    class Meta:
        proxy = True
        managed = True

    @classmethod
    def create_bet(cls, user: User, event: Event, amount: int, forecast: bool,
                   desc: str = None) -> BetModel:
        """
        This method creating bet for event by user
        :param user: Player (User)
        :param event: Event (Event)
        :param amount: Bid amount (int)
        :param forecast: Forecast event (bool)
        :param desc: [Optional] Bet description (str)
        :return: Bet (BetModel)
        """
        if cls.objects.filter(user=user, event=event).exists():
            raise PermissionDenied("A bet on this event from {0} already exists".format(user))

        bet = cls(
            user=user,
            event=event,
            amount=amount,
            forecast=forecast,
            desc=desc
        )

        bet._add_history_item(amount)

        # Not saving if signal handler raises exception
        try:
            with transaction.atomic():
                bet.save()
                created_bet.send(cls, bet=bet)
        except DatabaseError as e:
            logger.error(e)

        return bet

    def bet_increase(self, amount) -> BetModel:
        """
        This method increasing bet
        :param amount: New bet amount
        :return: Bet (BetModel)
        """
        if amount < self.amount:
            raise ValueError("New amount cannot less than previous")
        self.amount += amount
        self.updated_date = timezone.now()
        self._add_history_item(amount)

        # Not saving if signal handler raises exception
        try:
            with transaction.atomic():
                self.save()
                increased_bet.send(self.__class__, bet=self)
        except DatabaseError as e:
            logger.error(e)
        return self

    def bet_withdrawn(self, user: User) -> BetModel:
        """
        This method marked bet as withdrawn
        :param user: initiator (User)
        :return: Bet (BetModel)
        """
        if user != self.user and not user.is_staff:
            raise PermissionDenied("Only bet author or administrator can mark bet as withdrawn")

        self.is_withdrawn = True

        # Not saving if signal handler raises exception
        try:
            with transaction.atomic():
                self.save()
                withdrawn_bet.send(self.__class__, bet=self)
        except DatabaseError as e:
            logger.error(e)
        return self

    def _add_history_item(self, amount: int) -> BetModel:
        """
        This method writing amount and timestamp to bet history
        :param amount: Bet amount (int)
        :return: Bet (Bet)
        """
        self.history.append({
            "timestamp": timezone.now(),
            "amount": amount
        })
        return self
