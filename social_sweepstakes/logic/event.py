from django.contrib.auth import get_user_model
from django.core.exceptions import PermissionDenied
from django.utils import timezone

from social_sweepstakes.models import EventModel, ACCEPTED_BETS, END_EVENT, ProofModel, IN_CHECKING
from social_sweepstakes.signals import published_event, closing_event, need_checking_event

User = get_user_model()


class Event(EventModel):
    class Meta:
        proxy = True
        managed = True

    def published_event(self, user: User, end_bets_date: timezone.datetime) -> EventModel:
        """
        This method published event
        :param user: user initiator
        :param end_bets_date: datetime of end bets accepting
        :return: Updated event (Event)
        """
        if not user.is_staff:
            raise PermissionDenied("This action can performing only administrator")

        if end_bets_date < timezone.now():
            raise ValueError("End bets time can not be in the past")

        self.status = ACCEPTED_BETS
        self.end_bets_date = end_bets_date
        self.published_date = timezone.now()

        published_event.send(self.__class__, event=self)
        self.save()
        return self

    def closing_event(self, user: User, result: bool) -> EventModel:
        """
        This method closing event
        :param result: Event result. True - author wins. False - author lose.
        :param user: user initiator
        :return: Updated event (Event)
        """
        if not user.is_staff:
            raise PermissionDenied("This action can performing only administrator")

        self.status = END_EVENT
        self.end_date = timezone.now()
        self.result = result

        closing_event.send(self.__class__, event=self, winning_bets=self.winning_distribution())
        self.save()
        return self

    def load_proofs(self, user: User, proofs: [ProofModel]) -> [ProofModel]:
        """
        This method bulk saving proofs by user and updating event
        :param user: proofs author
        :param proofs: list proofs object (not saved)
        :return: Proofs list (list(Proof))
        """
        if user != self.user and not user.is_staff:
            raise PermissionDenied("Loading proofs can performing only event author or administrator")

        for proof in proofs:
            if not proof.link and not proof.file:
                raise ValueError("Proof must contain content (link or file)")
            proof.event = self

        proofs = ProofModel.objects.bulk_create(proofs)
        self.status = IN_CHECKING

        need_checking_event.send(self.__class__, event=self)
        self.save()
        return proofs

    def winning_distribution(self):
        """
        This method distributes bet and returning bet
        :return: Winning bets (QS(Bet))
        """

        winners = self.bets.filter(forecast=self.result)

        for bet in winners:
            bet.gain = bet.get_gain
            bet.save()

        return winners
