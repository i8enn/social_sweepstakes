from social_sweepstakes.models import ProofModel
from social_sweepstakes.settings import conf


class Proof(ProofModel):
    class Meta:
        proxy = True
        managed = True

    @staticmethod
    def check_file_ext(filename: str, stilly=False) -> bool or None:
        """
        This function validate file extension by filename.
        Raises exception if filename not in PROOF_ALLOWED_FILE_EXTENSIONS
        If stilly == True - nothing raises - just returns False
        :param filename: file name (str)
        :param stilly: Stilly mode (bool)
        :return: Validation result (bool or None)
        """
        ext = filename.split('.')[-1]
        if ext not in conf.PROOF_ALLOWED_FILE_EXTENSIONS:
            if not stilly:
                raise TypeError("File can't be with \"{0}\" extension. Allowed extensions: {1}".format(
                    ext, conf.PROOF_ALLOWED_FILE_EXTENSIONS))
            return False
        return True
