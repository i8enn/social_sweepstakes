from django.dispatch import Signal


# Event signals
published_event = Signal(providing_args=['event'])
closing_event = Signal(providing_args=['event', 'winning_bets'])

need_checking_event = Signal(providing_args=['event'])


# Bet signals
created_bet = Signal(providing_args=['bet'])
increased_bet = Signal(providing_args=['bet'])
withdrawn_bet = Signal(providing_args=['bet'])
