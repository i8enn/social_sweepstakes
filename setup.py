#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

from setuptools import find_packages, setup


def _get_version() -> list:
    import re
    with open(os.path.abspath(os.path.join(os.path.dirname(__file__), 'version')), 'r') as f:
        version = f.read().rstrip()
    cleared_version = re.findall(r"(\d\.[\d|\.\d]+-\d*-.*|\d\.[\d|\.\d]+)", version)[0]
    return cleared_version.split('-')


with open(os.path.join(os.path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()

os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name="social_sweepstakes",
    version="-".join(_get_version()),
    license='APACHE',
    description='Test application for Bravobet company',
    long_description=README,
    long_description_content_type='text/markdown',
    url="https://gitlab.com/i8enn/social_sweepstakes/",
    author="Ivan Galin",
    author_email="gin.volgograd@gmail.com",
    packages=find_packages(exclude=['tests*']),
    include_packages_data=True,
    install_requires=['Django>=1.11', 'jsonfield==2.0.2'],
    python_requires=">=3.5",  # FIXME
    zip_safe=False,
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Framework :: Django :: 1.11',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Internet :: WWW/HTTP',
    ],
)
