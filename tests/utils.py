class CatchSignal:
    """
    This class provides comfortable testing django signals
    """
    def __init__(self, signal):
        """
        Initial class
        :param signal: Target signal for testing
        """
        self.signal = signal
        self.signal_kwargs = {}

        def handler(sender, **kwargs):
            self.signal_kwargs.update(kwargs)

        self.handler = handler

    def __enter__(self) -> dict:
        """
        Connecting signal at use context manager
        :return: Signal kwargs (dict)
        """
        self.signal.connect(self.handler)
        return self.signal_kwargs

    def __exit__(self, exc_type, exc_value, tb) -> None:
        """
        Disconnect signal after exit from context manager
        :param exc_type: inherit
        :param exc_value: inherit
        :param tb: inherit
        :return: None
        """
        self.signal.disconnect(self.handler)
