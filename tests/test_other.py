import pytest
import re
import pkg_resources


class TestGettingVersion(object):
    @pytest.mark.parametrize('version', [
        "0.1",
        "0.1.4.2",
        "0.1-34-g3812e91",
        "0.1.4.2-34-g3812e91"
    ], ids=['release', 'long-release', 'dev', 'long-dev'])
    def test_parse_version(self, version):
        assert version
        # Version contain only int or '.'/'-'
        regexp_res = re.findall(r"(\d\.[\d|\.\d]+-\d*-.*|\d\.[\d|\.\d]+)", version)
        assert regexp_res

    def test__get_version(self):
        version = "-".join(pkg_resources.get_distribution('social_sweepstakes').version)
        assert version
