import os

import pytest
from django.contrib.auth import get_user_model
from django.utils import timezone

from social_sweepstakes.logic import Event, Proof
from tests.conftest import TESTING_MEDIA_DIR

User = get_user_model()


@pytest.fixture()
def user(transactional_db):
    return User.objects.create(
        username="Test",
        email="test@example.com",
        first_name="Test",
        last_name="For test",
        password="testpass",
    )


@pytest.fixture()
def another_user(transactional_db):
    return User.objects.create(
        username="AnotherTest",
        email="another@example.com",
        first_name="Another user",
        last_name="For test",
        password="anothertestpass",
    )


@pytest.fixture()
def user_staff(transactional_db):
    user = User.objects.create(
        username="TestStaff",
        email="staff@example.com",
        first_name="Staff",
        last_name="For test",
        password="teststaffpass",
    )
    user.is_staff = True
    user.save()
    return user


@pytest.fixture()
def players_users(transactional_db):
    return [
        User.objects.create(
            username="Player #{0}".format(i),
            email="player{0}@example.com".format(i),
            first_name="Test Player #{0}".format(i),
            last_name=i,
            password="tp{0}".format(i),
            is_active=True
        )
        for i in range(1, 11)
    ]


@pytest.fixture()
def test_event(transactional_db, user):
    return Event.objects.create(
        user=user,
        title="Test event",
        desc="Test event",
        end_date=timezone.now() + timezone.timedelta(days=30),
        balance=0
    )


@pytest.fixture()
def proofs(test_event):
    return [
        Proof(
            title="Proof #{0}".format(i),
            desc="Proof description",
            link="https://placekitten.com/200/300" if i <= 2 else None,
            file=os.path.join(TESTING_MEDIA_DIR, 'proofs/test.jpg') if i > 2 else None
        ) for i in range(1, 5)
    ]
