import pytest

from social_sweepstakes.logic import Proof


class TestProofLogic(object):

    def test_check_file_ext(self):
        assert Proof.check_file_ext('testfile.jpeg')

    def test_check_file_ext_raises(self):
        with pytest.raises(TypeError):
            assert Proof.check_file_ext('badext.exe')