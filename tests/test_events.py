import pytest
from django.core.exceptions import PermissionDenied
from django.utils import timezone

from social_sweepstakes.logic import Event, Proof, Bet
from social_sweepstakes.models import (
    ACCEPTED_BETS,
    END_EVENT, IN_CHECKING)
from social_sweepstakes.settings import conf
from social_sweepstakes.signals import (
    published_event, closing_event, need_checking_event
)

from .utils import CatchSignal
from .fixtures import test_event, user, user_staff, another_user, players_users, proofs


class TestEventLogic(object):

    def test_published_event(self, test_event: Event, user_staff):
        event = Event.objects.get(user=test_event.user, title=test_event.title)
        event = event.published_event(user_staff, event.end_date - timezone.timedelta(days=15))

        assert event.status == ACCEPTED_BETS
        assert event.end_bets_date == event.end_date - timezone.timedelta(days=15)

    def test_published_event_with_non_staff_user(self, test_event: Event, user):
        with pytest.raises(PermissionDenied, match="only administrator"):
            test_event.published_event(user, timezone.now() + timezone.timedelta(days=30))

    def test_published_event_with_past_end_bets_date(self, test_event: Event, user_staff):
        with pytest.raises(ValueError, match="can not be in the past"):
            test_event.published_event(user_staff, timezone.now() - timezone.timedelta(days=1))

    def test_sending_published_event_signal(self, test_event: Event, user_staff):
        with CatchSignal(published_event) as signal_kwargs:
            test_event.published_event(user_staff, timezone.now() + timezone.timedelta(days=15))

        assert signal_kwargs.get('event') == test_event

    def test_closing_event(self, test_event: Event, user_staff):
        event = Event.objects.get(user=test_event.user, title=test_event.title).closing_event(user_staff, True)

        assert event.status == END_EVENT
        assert event.result is True

    def test_closing_event_with_non_staff_user(self, test_event: Event, user):
        with pytest.raises(PermissionDenied, match="only administrator"):
            test_event.closing_event(user, False)

    def test_sending_closing_event_signal(self, test_event: Event, user_staff):
        with CatchSignal(closing_event) as signal_kwargs:
            test_event.closing_event(user_staff, False)

        assert signal_kwargs.get('event') == test_event

    def test_load_proofs(self, test_event: Event, user, proofs):
        _proofs = test_event.load_proofs(user, proofs)
        event = Event.objects.get(pk=test_event.pk)

        assert _proofs
        for proof in _proofs:
            assert proof.event == event
            assert proof.link or proof.file

        assert event.status == IN_CHECKING

    def test_bulk_load_proofs(self, django_assert_num_queries, test_event: Event, user, proofs):
        with django_assert_num_queries(len(proofs)):
            test_event.load_proofs(user, proofs)

    def test_load_proof_with_another_user(self, test_event: Event, another_user, proofs):
        with pytest.raises(PermissionDenied, match="only event author or administrator"):
            test_event.load_proofs(another_user, proofs)

    def test_load_empty_proofs(self, test_event: Event, user):
        with pytest.raises(ValueError, match="must contain content"):
            test_event.load_proofs(user, [
                Proof(event=test_event, title=i, desc=i)
                for i in range(1, 3)
            ])

    def test_sending_need_checking_proof_signal(self, test_event: Event, user, proofs):
        with CatchSignal(need_checking_event) as signal_kwargs:
            test_event.load_proofs(user, proofs)

        assert signal_kwargs.get('event') == test_event

    def test_winning_distribution(self, test_event: Event, user, user_staff, players_users):
        test_event.published_event(user_staff, timezone.now() + timezone.timedelta(days=15))
        bets = []
        for player in players_users:
            bets.append(Bet.create_bet(
                player,
                test_event,
                100 * int(player.last_name),
                True if int(player.last_name) <= 4 else False
            ))

        with CatchSignal(closing_event) as signal_kwargs:
            test_event.closing_event(user_staff, False)

        winning_bets = signal_kwargs.get('winning_bets')

        assert len(winning_bets) == 6
        gain_size = test_event.get_clear_gains()[0] / winning_bets.count()

        for bet in winning_bets:
            # Check gain
            assert bet.gain - bet.amount == int(gain_size)

            # Check gain with commission
            assert bet.gain_with_commission == bet.gain - ((bet.gain * conf.WINNING_COMMISSION_PERCENT) * 100)
