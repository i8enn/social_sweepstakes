import pytest
from django.core.exceptions import PermissionDenied
from django.contrib.auth import get_user_model

from social_sweepstakes.logic import Bet, Event
from social_sweepstakes.signals import created_bet, increased_bet, withdrawn_bet
from social_sweepstakes.settings import conf
from tests.utils import CatchSignal
from .fixtures import another_user, test_event, user

User = get_user_model()


@pytest.fixture()
def test_bet(transactional_db, test_event, another_user):
    return Bet.create_bet(
        user=another_user,
        event=test_event,
        amount=100,
        forecast=True,
        desc="Test bet"
    )


class TestBetSettings(object):
    def test_default_bet_forecast(self):
        assert conf.DEFAULT_BET_FORECAST == False

    def test_winning_commission_percent(self):
        assert conf.WINNING_COMMISSION_PERCENT == 10


class TestBetLogic(object):

    def test_create_bet(self, another_user: User, test_event: Event):
        bet = Bet.create_bet(another_user, test_event, 100, True, "Test bet")

        assert bet.user == another_user
        assert bet.event == test_event
        assert bet.amount == 100
        assert bet.forecast is True
        assert bet.desc == "Test bet"
        assert bet.event.balance == test_event.balance

    def test_sending_created_bet_signal(self, another_user: User, test_event: Event):
        with CatchSignal(created_bet) as signal_kwargs:
            bet = Bet.create_bet(another_user, test_event, 100, True, "Test bet")

        assert signal_kwargs.get('bet') == bet

    def test_create_bet_again(self, another_user: User, test_event: Event):
        Bet.create_bet(another_user, test_event, 100, True, "Test bet")
        with pytest.raises(PermissionDenied, match="already exists"):
            Bet.create_bet(another_user, test_event, 100, True, "Test bet")

    def test_bet_increase(self, another_user: User, test_event: Event, test_bet: Bet):
        bet = test_bet.bet_increase(200)

        assert bet
        assert bet.amount == bet.history[-1]['amount'] + bet.history[-2]['amount']
        assert 200 == bet.history[-1]['amount']
        assert bet.updated_date
        assert bet.event.balance - (bet.amount - test_bet.amount) == test_event.balance

    def test_bet_increasing_with_less_than_prev(self, test_bet: Bet):
        with pytest.raises(ValueError, match="less"):
            test_bet.bet_increase(1)

    def test_sending_increased_bet_signal(self, test_bet: Bet):
        with CatchSignal(increased_bet) as signal_kwargs:
            bet = test_bet.bet_increase(200)

        assert signal_kwargs.get('bet') == bet

    def test_raises_in_increased_bet_signal(self, test_bet: Bet):
        def raise_handler(*args, **kwargs):
            raise NotImplementedError()

        increased_bet.connect(raise_handler)

        with pytest.raises(NotImplementedError):
            test_bet.bet_increase(200)

    def test_not_saving_if_handler_raises_in_increased_bet_signal(self, test_bet: Bet):
        def raise_handler(*args, **kwargs):
            raise NotImplementedError()

        increased_bet.connect(raise_handler)

        old_amount = test_bet.amount
        with pytest.raises(NotImplementedError):
            test_bet.bet_increase(200)

        bet = Bet.objects.get(pk=test_bet.pk)
        assert bet.amount == old_amount

    def test_rollback_handlers_with_raises_in_increased_bet_signal(self, test_bet: Bet):
        def raise_handler(*args, **kwargs):
            raise NotImplementedError()

        def transaction_handler(*args, **kwargs):
            instance = kwargs.get('bet')
            instance.amount = 1000

        increased_bet.connect(raise_handler)
        increased_bet.connect(transaction_handler)

        with pytest.raises(NotImplementedError):
            test_bet.bet_increase(200)
            assert test_bet.amount != 1000

    def test_bet_withdrawn(self, test_bet: Bet):
        test_bet.bet_withdrawn(test_bet.user)
        assert test_bet.is_withdrawn

    def test_bet_withdrawn_with_not_author_bet(self, test_bet: Bet, user: User):
        with pytest.raises(PermissionDenied, match="author or administrator"):
            test_bet.bet_withdrawn(user)

    def test_sending_withdrawn_bet_signal(self, test_bet: Bet):
        event_balance = test_bet.event.balance
        with CatchSignal(withdrawn_bet) as signal_kwargs:
            test_bet.bet_withdrawn(test_bet.user)

        bet = signal_kwargs.get('bet')
        assert bet == test_bet
        assert bet.is_withdrawn
