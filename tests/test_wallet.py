from unittest import mock
import pytest
from django.core.exceptions import FieldDoesNotExist

from social_sweepstakes.wallet.logic import Wallet, Transactions
from social_sweepstakes.logic.event import Event
from social_sweepstakes.logic.bet import Bet
from social_sweepstakes.settings import conf
from tests.fixtures import user, another_user, user_staff, test_event
from social_sweepstakes.signals import created_bet, increased_bet, withdrawn_bet
from tests.utils import CatchSignal


@pytest.fixture()
def test_wallets(transactional_db, user, another_user):
    return {Wallet.objects.create(user=user, balance=1000),
            Wallet.objects.create(user=another_user, balance=500)}


@pytest.fixture()
def test_transaction(transactional_db, test_wallets):
    wallet_from, wallet_to = test_wallets
    transaction = Transactions.create_transaction(wallet_from, wallet_to, 500)
    assert transaction
    return transaction


@pytest.fixture()
def test_bet(transactional_db, test_wallets, test_event):
    wallet_from, _ = test_wallets
    return Bet.create_bet(
        wallet_from.user,
        test_event,
        100,
        forecast=True
    )


class TestTransactionSettings(object):
    def test_default_wallet_can_overdraft(self):
        assert conf.SOC_SWEEP_WALLET_CAN_OVERDRAFT is False

    def test_setting_override_wallet_can_overdraft(self, settings):
        settings.SOC_SWEEP_WALLET_CAN_OVERDRAFT = True
        assert conf.SOC_SWEEP_WALLET_CAN_OVERDRAFT is True


class TestTransactionLogic(object):
    def test_create_transaction_between_wallets(self, test_wallets):
        wallet_from, wallet_to = test_wallets
        _from_balance = wallet_from.balance
        _to_balance = wallet_to.balance
        transaction = Transactions.create_transaction(wallet_from, wallet_to, 500)
        assert transaction.from_account.balance == _from_balance - transaction.amount
        assert transaction.to_account.balance == _to_balance + transaction.amount

    def test_create_transaction_with_non_balance_object(self, user):
        wallet = Wallet.objects.create(user=user, balance=1000)
        with pytest.raises(FieldDoesNotExist, match="not have a 'balance' field"):
            Transactions.create_transaction(user, wallet, 500)
        with pytest.raises(FieldDoesNotExist, match="not have a 'balance' field"):
            Transactions.create_transaction(wallet, user, 500)

    def test_create_transaction_with_event(self, test_wallets, test_event):
        wallet_from, _ = test_wallets
        _from_balance = wallet_from.balance
        _to_balance = test_event.balance
        transaction = Transactions.create_transaction(wallet_from, test_event, 500)
        assert transaction.from_account.balance == _from_balance - transaction.amount
        assert transaction.to_account.balance == _to_balance + transaction.amount

    def test_raise_create_transaction_with_null_balance_in_from_account(self, test_wallets):
        wallet_from, wallet_to = test_wallets
        with pytest.raises(ValueError, match="account insufficient funds for transaction"):
            Transactions.create_transaction(wallet_from, wallet_to, 100000)

    def test_create_transaction_with_null_balance_and_allow_wallet_overdraft(self, settings, test_wallets):
        settings.SOC_SWEEP_WALLET_CAN_OVERDRAFT = True

        wallet_from, wallet_to = test_wallets
        transaction = Transactions.create_transaction(wallet_from, wallet_to, 100000)
        assert transaction.from_account.balance < 0

    def test_rollback_transaction(self, test_transaction: Transactions):
        status = test_transaction.rollback_transaction()
        assert status


class TestTransactionLogicWithEventBets(object):
    def test_create_transaction_with_create_bet(self, test_wallets, test_event: Event):
        wallet_one, _ = test_wallets
        _from_balance = wallet_one.balance
        _to_balance = test_event.balance
        with CatchSignal(created_bet) as signal_kwargs:
            with mock.patch('social_sweepstakes.wallet.handlers.wallets.create_or_update_bet_handler', autospec=True) as mocked_handler:
                created_bet.connect(mocked_handler, Bet)
                bet = Bet.create_bet(wallet_one.user, test_event, 100, forecast=True)
            assert mocked_handler.call_count == 1
        transaction = Transactions.objects.get(from_account_id=wallet_one.pk, to_account_id=test_event.pk)
        assert transaction
        assert transaction.from_account == wallet_one
        assert transaction.to_account == test_event
        assert transaction.amount == bet.amount
        assert transaction.from_account.balance == _from_balance - transaction.amount
        assert transaction.to_account.balance == _to_balance + transaction.amount

    def test_create_transaction_with_increase_bet(self, test_bet: Bet):
        _from_balance = test_bet.user.wallet.balance
        _to_balance = test_bet.event.balance
        with CatchSignal(increased_bet) as signal_kwargs:
            with mock.patch('social_sweepstakes.wallet.handlers.wallets.create_or_update_bet_handler', autospec=True) as mocked_handler:
                increased_bet.connect(mocked_handler, Bet)
                bet = test_bet.bet_increase(100)
            assert mocked_handler.call_count == 1
        transaction = Transactions.objects.filter(
            from_account_id=test_bet.user.wallet.pk, to_account_id=test_bet.event.pk).last()
        assert transaction
        assert transaction.from_account == bet.user.wallet
        assert transaction.to_account == bet.event
        assert transaction.amount == bet.amount - bet.history[-1]['amount']
        assert transaction.from_account.balance == _from_balance - transaction.amount
        assert transaction.to_account.balance == _to_balance + transaction.amount

    def test_create_transaction_with_withdrawn_bet(self, test_bet: Bet):
        _from_balance = test_bet.event.balance
        _to_balance = test_bet.user.wallet.balance
        with CatchSignal(increased_bet) as signal_kwargs:
            with mock.patch('social_sweepstakes.wallet.handlers.wallets.withdrawn_bet_handler', autospec=True) as mocked_handler:
                withdrawn_bet.connect(mocked_handler, Bet)
                bet = test_bet.bet_withdrawn(test_bet.user)
            assert mocked_handler.call_count == 1
        transaction = Transactions.objects.filter(
            from_account_id=test_bet.event.pk, to_account_id=test_bet.user.wallet.pk).last()
        assert transaction
        assert transaction.from_account == test_bet.event
        assert transaction.to_account == test_bet.user.wallet
        assert transaction.amount == bet.amount
        assert transaction.from_account.balance == _from_balance - transaction.amount
        assert transaction.to_account.balance == _to_balance + transaction.amount
